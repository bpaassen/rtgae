# Recursive Tree Grammar Autoencoders

Copyright (C) 2020  
Benjamin Paaßen  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

## Introduction

This repository contains a reference implementation of
_Recursive Tree Grammar Autoencoders_ (RTG-AEs) as described in the paper

* Paaßen, Koprinska, and Yacef (2020). Recursive Tree Grammar Autoencoders.
  [arXiv:2012.02097][rtgae]

If you wish to use this code in academic work, please cite this paper.

In particular, this repository contains implementations of all methods used
in the experiments, including a reference implementation of RTG-AEs, iPython
notebooks for all experiments, the datasets for all experiments, the reference
results as reported in the paper, and auxiliary files.

In the remainder of this README, we provide the required packages to run the
modules of this repository, a quickstart guide how to train your own RTG-AEs
on novel datasets, instructions how to reproduce the experiments reported in
the paper, and a detailed list of all enclosed files with explanations.

## Installation Instructions

All software enclosed in this repository is written in Python 3. To train
RTG-AEs, you additionally require [PyTorch][pytorch] (Version >= 1.4.0;
torchvision or cuda are not required) and [numpy][numpy] (Version >= 1.17).
This also suffices to run the grammar variational autoencoder implementation
(GVAE; [Kusner et al., 2017][Kus2017]).

To run the variational autoencoder for directed acyclic graphs (D-VAE;
[Zhang et al., 2019][Zhang2019]), you additionally require
[python-igraph][igraph] (Version >= 0.7.1), and [tqdm][tqdm] (Version >= 4.45).

To run the tree echo state autoencoder (TES-AE;
[Paaßen, Koprinska, and Yacef, 2020][Paa2020]), you require [numpy][numpy]
(Version >= 1.17) and [scikit-learn][sklearn] (Version >= 0.21.3).

To run the boolean, expressions, and pysort experiments, you require all the
aforementioned packages and additionally [edist][edist] (Version >= 1.1.0),
[joblib][joblib] (Version >= 0.13.2), and [matplotlib][matplotlib]
(Version >= 3.1.3). To run optimization in the expressions data set, you
additionally require the [cma][cma] package.

To run the SMILES experiment, you require all these packages, and additionally
the [ANTLR4 runtime for Python3][antlr4] (Version >=4.7.2) and [rdkit][rdkit]
(Version >= 2019.03.3).

Except for rdkit, all these packages are available on pip.

## Quickstart guide

To train a RTG-AE on your own tree data, you first need to convert your trees
into node list/adjacency list format, i.e. you need for each tree two Python
lists, the first containing the node labels (here assumed as strings) in depth
first search order, the second containing an adjacency list of the tree. For
example, the tree a(b, c(d)) would correspond to the node list
`['a', 'b', 'c', 'd']` and the adjacency list `[[1, 2], [], [3], []]` because
the node 0 (a) has nodes 1 and 2 (b and c) as children, node 1 has no children,
node 2 has node 3 as child, and node 3 has no children. To automatically
convert your trees to this format, you may refer to the `tree.to_list_format`
function for inspiration.

Next, you require a regular tree grammar for your dataset. A regular tree
grammar consists of four ingredients: An alphabet of possible tree labels,
an alphabet of auxiliary/nonterminal symbols, a starting nonterminal symbol,
and, most importantly, a set of grammar rules of the form
$`A \to x(B_1, \ldots, B_k)`$, where $`x`$ is a tree label and
$`A, B_1, \ldots, B_k`$ are nonterminal symbols. A tree can then be generated
by initializing the tree with the starting nonterminal and subsequently
replacing the leftmost nonterminal $`A`$ in the tree with the right-hand-side
of a rule that has $`A`$ on the left-hand-side, until no nonterminals are left.
In Python, we format the rule set as a dictionary, which maps left hand sides
to a list of possible right-hand-sides, where each right-hand-side in turn is a
tuple `(x, [B_1, ..., B_k])` of the label `x` and the right-hand-side
nonterminals `[B_1, ..., B_k]`. A grammar for our example above could look like
this.

```Python
import tree_grammar

alphabet     = ['a', 'b', 'c', 'd']
nonterminals = ['S', 'S1', 'S2']
start        = 'S'
rules        = {
    'S'  : [('a', ['S1', 'S2'])],
    'S1' : [('b', []), ('d', [])],
    'S2' : [('c', ['S1'])]
}
grammar      = tree_grammar.TreeGrammar(alphabet, nonterminals, start, rules)
```

Our example tree can be generated by first replacing `S` with `a(S1 S2)`, then
`S1` with `b` (note that the child list is empty), then `S2` with `c(S1)` and
finally `S1` with `d`.

Now, assume that the list of all trees is called `training_data` and the
tree grammar is called `grammar`. Then, a RTG-AE can be trained with the
following template.

```Python
import random
import torch
import recursive_tree_grammar_auto_encoder as rtgae

model = rtgae.TreeGrammarAutoEncoder(grammar, dim = 100, dim_vae = 8)
optimizer = torch.optim.Adam(model.parameters(), lr=1E-3, weight_decay=0.)

for epoch in range(30000):
    optimizer.zero_grad()
    # sample a random tree from the training data
    i = random.randrange(len(training_data))
    nodes, adj = training_data[i]
    # compute the loss on it
    loss = model.compute_loss(nodes, adj, beta = 0.01, sigma_scaling = 0.1)
    # compute the gradient
    loss.backward()
    # perform an optimizer step
    optimizer.step()
```

Note that the hyper-parameters used here are values fitting for the expressions
and boolean experiment but may need to be adjusted for your data. Further, you
may wish to compute the loss not on a single tree but on a minibatch of trees,
keep track of the training loss over time, and so on. This template is just the
barebones minimum to get the model to run.

After training, you can encode a tree as a vector via the command
`_, h = model.encode(nodes, adj)`. `h` will have the dimensionality `dim_vae`,
which was 8 in the example above.
You can also generate novel trees from `dim_vae` dimensional random normal
vectors via the code

```Python
import torch

h = torch.randn(dim_vae)
nodes, adj, _ = model.decode(h, max_size = 100)
```

It is typically recommendable to use the optional `max_size` argument to
prevent endless loops.

## Reproducing the experiments

The results of the experiments in terms of root mean square autoencoding error
(as measured by the tree edit distance; [Zhang and Shasha, 1989][Zhang1989])
as follows (∓ standard deviation).

| model     | boolean     | expressions | smiles          | pysort       |
| --------- | ----------- | ----------- | --------------- | ------------ |
| D-VAE     | 4.32 ∓ 0.41 | 5.84 ∓ 0.32 | 132.70 ∓ 57.02  | 70.67 ∓ 7.20 |
| GVAE      | 3.61 ∓ 0.51 | 5.84 ∓ 2.00 | 594.92 ∓ 5.99   | 61.44 ∓ 5.18 |
| TES-AE    | 2.62 ∓ 0.26 | 2.09 ∓ 0.18 | 581.08 ∓ 25.99  | 20.40 ∓ 4.27 |
| GRU-TG-AE | 1.98 ∓ 0.53 | 3.70 ∓ 0.31 | 482.88 ∓ 129.52 | 93.63 ∓ 1.44 |
| RTG-AE    | 0.83 ∓ 0.23 | 0.77 ∓ 0.23 | 111.14 ∓ 159.97 | 38.14 ∓ 3.63 |

For more detailed results, including runtimes, please refer to the `results`
folder, which contains detailed results for each experimental run in the form
of CSV tables. For summarization of the results, the `results/results_analysis`
notebook was used.

These results can be reproduced by running the `boolean`, `expressions`,
`smiles`, and `pysort` notebooks. Note that exact equality of the results
can not be expected due to different random seeds; also note that the
experiments may be quite time- and memory-consuming. To get a first impression,
the `boolean` and `expressions` notebooks are recommended, which should run in
several hours rather than days.

All data sets, as well as the hyperparameter selection and crossvalidation
folds (for smiles and pysort) are already prepared and will be exactly as in
the experiments reported in the paper. For the expressions and smiles data
sets, we additionally provide pre-trained models for the optimization part of
the experiments (except for the tree echo state auto-encoder, which takes too
much disk space and is quick to train). As such, no additional preparation on
your end should be necessary. The notebooks should run right away.

## Contents

The exact contents of this repository are as follows (in alphabetical order).

* `boolean_data.py` : A Python module to generate boolean expression trees.
* `boolean.ipynb` : The iPython notebook for the boolean experiment.
* `dvae.py` : The reference implementation for D-VAEs
  ([Zhang et al., 2019][Zhang2019]), only slightly altered to be compatible
  with tree inputs.
* `expressions_data.py` : A Python module to generate algebraic expression
  trees as defined by [Kusner et al. (2017)][Kus2017]. Also contains an
  implementation of the objective function suggested by
  [Kusner et al. (2017)][Kus2017].
* `expressions.ipynb` : The iPython notebook for the expressions experiment.
* `fpscores.pkl.gz` : An auxiliary file containing scores for certain
  chemical fragments.
* `grammar_vae.py` : The reference implementation for GVAEs
  ([Kusner et al., 2017][Kus2017]), ported from tensorflow to pytorch.
* `gru_tree_grammar_auto_encoder.py` : A pytorch implementation of tree grammar
  auto-encoders which are not recursive but recurrent for ablation study
  purposes.
* `LICENSE.md` : A copy of the GNU GPLv3 license.
* `pyproject.toml` : A dependency declaration file in [poetry][poetry] format.
* `pysort` : A directory containing the pysort dataset. The directory also
  contains an additional README file with information how this data was
  generated.
* `pysort_data.py` : A Python module containing auxiliary functions to load the
  `pysort` dataset.
* `pysort_hyper` : A directory containing additional Python sorting programs
  used for hyperparameter optimization.
* `pysort.ipynb` : The iPython notebook for the pysort experiment.
* `README.md` : This file.
* `recursive_tree_grammar_auto_encoder.py` : The reference implementation of
  RTG-AEs.
* `results` : A directory containing all results generated by the `boolean`,
  `expressions`, `smiles`, and `pysort` notebooks as CSV tables.
* `sascorer.py` : An implementation of the objective function for the SMILES
  dataset as suggested by [Kusner et al. (2017)][Kus2017] with only slight
  modifications to be compatible with Python3.
* `single_step_gru.py` : An auxiliary wrapper for the pytorch GRU to be easier
  to use.
* `smiles_data.py` : A Python module containing auxiliary functions to load and
  score the SMILES dataset.
* `smiles_data.zip` : The SMILES data set as provided by
  [Kusner et al. (2017)][Kus2017]. This needs to be unpacked for the smiles
  experiment.
* `smiles.ipynb` : The iPython notebook for the SMILES experiment.
* `smiles_parser` : A directory with auxiliary files to parse SMILES strings to
  syntax trees. This uses [ANTLR4][antlr4].
* `tree_echo_state_auto_encoder.py` : The reference implementation of tree echo
  state autoencoders (anonymous, 2020; please find the paper attached).
* `tree_grammar.py` : A Python module for regular tree grammars.
* `tree.py` : A Python module for tree handling.

## Licenses

The code provided by Benjamin Paaßen is licensed under the [GNU GPLv3][GPLv3].
The `sascorer.py` file is redistributed according to a special Novartis license
(refer to the file). The `dvae.py` code is modified and redistributed according
to the MIT License.

This README file and all documentation enclosed in this repository is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.

## Literature

* Paaßen, Koprinska, and Yacef (2020). Recursive Tree Grammar Autoencoders. [arXiv:2012.02097][rtgae]
* Paaßen, Koprinska, and Yacef (2020). Tree Echo State Autoencoders with
  Grammars. Proceedings of the IJCNN 2020. in press. [Link][Paa2020]
* Kusner, Paige, and Hernández-Lobato (2017). Grammar Variational Autoencoder.
  Proceedings of ICML 2017. [Link][Kus2017]
* Zhang, and Shasha (1989). Simple Fast Algorithms for the Editing Distance
  between Trees and Related Problems. SIAM Journal on Computing, 18(6),
  1245-1262. doi:[10.1137/0218082][Zhang1989]
* Zhang, Jiang, Cui, Garnett, and Chen (2019). D-VAE: A Variational Autoencoder
  for Directed Acyclic Graphs. Proceedings of NeurIPS 2019. [Link][Zhang2019]

[rtgae]:https://arxiv.org/abs/2012.02097
[Paa2020]:https://arxiv.org/abs/2004.08925 "Paaßen, Koprinska, and Yacef (2020). Tree Echo State Autoencoders with Grammars. Proceedings of the IJCNN 2020. in press."
[Kus2017]:http://proceedings.mlr.press/v70/kusner17a.html "Kusner, Paige, and Hernández-Lobato (2017). Grammar Variational Autoencoder. Proceedings of ICML 2017."
[Zhang1989]:https://doi.org/10.1137/0218082 "Zhang, and Shasha (1989). Simple Fast Algorithms for the Editing Distance between Trees and Related Problems. SIAM Journal on Computing, 18(6), 1245-1262. doi:10.1137/0218082"
[Zhang2019]:http://papers.nips.cc/paper/8437-d-vae-a-variational-autoencoder-for-directed-acyclic-graphs "Zhang, Jiang, Cui, Garnett, and Chen (2019). D-VAE: A Variational Autoencoder for Directed Acyclic Graphs. Proceedings of NeurIPS 2019."
[antlr4]:https://www.antlr.org/ "ANTLR homepage."
[edist]:https://gitlab.ub.uni-bielefeld.de/bpaassen/python-edit-distances "edist homepage."
[igraph]:https://igraph.org/python/ "igraph homepage."
[joblib]:https://github.com/joblib/joblib "joblib GIT repository."
[matplotlib]:https://matplotlib.org/ "matplotlib homepage."
[numpy]:https://numpy.org/ "numpy homepage."
[poetry]:https://python-poetry.org "poetry homepage."
[pytorch]:https://pytorch.org/ "PyTorch homepage."
[rdkit]:https://www.rdkit.org/ "RDKit homepage."
[sklearn]:https://scikit-learn.org/stable/ "scikit-learn homepage"
[tqdm]:https://github.com/tqdm/tqdm "tqdm homepage."
[cma]:https://github.com/CMA-ES/pycma "cma homepage."
[GPLv3]: https://www.gnu.org/licenses/gpl-3.0.en.html "The GNU General Public License Version 3"
