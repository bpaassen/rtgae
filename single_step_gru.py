"""
Implements a GRU interface to process only a single time step, making the
interface similar to the linear layer.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import torch

class SingleStepGRU(torch.nn.Module):

    def __init__(self, dim):
        super(SingleStepGRU, self).__init__()
        self._gru = torch.nn.GRU(dim, dim)

    def forward(self, h):
        x = torch.zeros(1, 1, len(h))
        h = h.unsqueeze(0).unsqueeze(1)
        h, _ = self._gru(x, h)
        h = h[0, 0, :]
        return h
