"""
Implements the expressions dataset of Kusner et al. (2017).
"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random
import numpy as np
import tree
import tree_grammar

# set up grammar of the expressions domain. Note that this grammar does
# _not_ contain information about the specific shape of the training data
# imposed by Kusner et al., but permits any valid expression over the
# alphabet.

alphabet = {'+' : 2, '*' : 2, '/' : 2, 'sin' : 1, 'exp' : 1, 'x' : 0, '1' : 0, '2' : 0, '3' : 0}
nonterminals = ['S']
start    = 'S'
rules    = { 'S' : [
    ('+', ['S', 'S']),
    ('*', ['S', 'S']),
    ('/', ['S', 'S']),
    ('sin', ['S']),
    ('exp', ['S']),
    ('x', []),
    ('1', []),
    ('2', []),
    ('3', [])
]}

grammar = tree_grammar.TreeGrammar(alphabet, nonterminals, start, rules)

def sample_tree():
    """ Returns a tree representing an algebraic expression with the following
    shape +(binary, +(unary, combination)), where

    *   'combination' is sampled as either a 'binary', a 'unary', or a
        unary operator with a 'binary' as child.
    *   'binary' is sampled as either a 'literal', or as a binary operator
        (+, *, or /) with a 'literal' as left and right argument.
    *   'unary' is sampled as either a 'literal' or as a unary operator
        (sin or exp) with a 'literal' as argument.
    *   'literal' is sampled as either x, 1, 2, or 3.

    In other words, the actual trainin gdata is sampled from the following
    grammar (where operators are denoted infix to ease reading):

    Expression -> Binary + Unary + Combination
    Binary -> Literal | Literal + Literal | Literal * Literal | Literal / Literal
    Unary -> Literal | exp(Literal) | sin(Literal)
    Combination -> Binary | Unary | exp(Binary) | sin(Binary)
    Literal -> 1 | 2 | 3 | x

    A typical example expression that results is
    +(/(1, 3), +(x, sin(*(x, 2)))).

    All random choices are done with uniform probabilities.

    Returns
    -------
    nodes: list
        The node list of the generated tree.
    adj: list
        The adjacency list of the generated tree.

    """
    # start off by sampling the binary
    binary = _sample_binary()
    # then the unary
    unary  = _sample_unary()
    # then the combination
    combination = _sample_combination()
    # build the final expression and return it
    expr = tree.Tree('+', [binary, tree.Tree('+', [unary, combination])])
    return expr.to_list_format()

def _sample_combination():
    r = random.randrange(4)
    if r == 0:
        return _sample_binary()
    if r == 1:
        return _sample_unary()
    children = [_sample_binary()]
    if r == 2:
        return tree.Tree('exp', children)
    if r == 3:
        return tree.Tree('sin', children)

def _sample_binary():
    r = random.randrange(4)
    if r == 0:
        return _sample_literal()
    left = _sample_literal()
    right = _sample_literal()
    children = [left, right]
    if r == 1:
        return tree.Tree('+', children)
    if r == 2:
        return tree.Tree('*', children)
    if r == 3:
        return tree.Tree('/', children)

def _sample_unary():
    r = random.randrange(3)
    if r == 0:
        return _sample_literal()
    children = [_sample_literal()]
    if r == 1:
        return tree.Tree('exp', children)
    if r == 2:
        return tree.Tree('sin', children)

def _sample_literal():
    r = random.randrange(4)
    return tree.Tree('123x'[r])

def to_algebraic_string(nodes, adj, i = 0):
    """ Transforms a given tree representation of an algebraic expression into
    a more readable string form.

    Note that this method only works for inputs conforming to the structure of
    sample_tree(). Otherwise, the bracketing may be wrong.

    Parameters
    ----------
    nodes: list
        The node list of the input tree.
    adj: list
        The adjacency list of the input tree.
    i: int (optional, default = 0)
        The root index of the input tree.

    Returns
    -------
    str: string
        The string representation of the input tree.
    """
    if nodes[i] == '+' or nodes[i] == '*' or nodes[i] == '/':
        return to_algebraic_string(nodes, adj, adj[i][0]) + ' ' + nodes[i] + ' ' + to_algebraic_string(nodes, adj, adj[i][1])
    if nodes[i] == 'sin' or nodes[i] == 'exp':
        return nodes[i] + '(' + to_algebraic_string(nodes, adj, adj[i][0]) + ')'
    else:
        return nodes[i]

def objective_function(nodes, adj):
    """ Returns the prediction error of the given expression compared to the
    ground truth expression 1/3+x+sin(x*x) as described by Kusner et al.
    (2017). This is the objective functiion for expression optimization.

    In more detail, the returned error is log(1 + MSE), where the MSE is
    computed on 1000 linearly spaced points in the range -10 to 10.

    Parameters
    ----------
    nodes: list
        The node list of the input expression tree.
    adj: list
        The adjacency list of the input expression tree.

    Returns
    -------
    loss: float
        The prediction error as descibed above.
    """
    # create inputs
    x = np.linspace(-10., 10., 1000)
    # compute ground-truth values
    y = 1. / 3. + x + np.sin(x * x)
    # compute predicted values
    y_pred = evaluate(nodes, adj, x)
    # return log(1 + MSE) as suggested by Kusner et al. (2017)
    return np.log(1. + np.mean((y - y_pred) ** 2))

def evaluate(nodes, adj, x, i = 0):
    """ Evaluates the given expression for the given x values.

    Parameters
    ----------
    nodes: list
        The node list of the input expression tree.
    adj: list
        The adjacency list of the input expression tree.
    x: array_like
        A single x value or an array of x values.

    Returns
    -------
    val: array_like
        The evaluation of the input expression for the given
        input with the same size as the input.
    """
    # evaluate children first
    children = []
    for j in adj[i]:
        children.append(evaluate(nodes, adj, x, j))
    # then evaluate the current node
    if nodes[i] == '+':
        return children[0] + children[1]
    if nodes[i] == '*':
        return children[0] * children[1]
    if nodes[i] == '/':
        return children[0] / children[1]
    if nodes[i] == 'exp':
        return np.exp(children[0])
    if nodes[i] == 'sin':
        return np.sin(children[0])
    if nodes[i] == 'x':
        return x
    if nodes[i] == '1':
        return np.ones_like(x)
    if nodes[i] == '2':
        return 2 * np.ones_like(x)
    if nodes[i] == '3':
        return 3 * np.ones_like(x)
    raise ValueError('Unknown symbol: %s' % nodes[i])
