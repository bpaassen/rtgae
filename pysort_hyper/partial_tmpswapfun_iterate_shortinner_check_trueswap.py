def sort ( a ):
    def swap(a, i):
        tmp = a[i]
        a[i] = a[i+1]
        a[i+1] = tmp
    for i in range(len(a)):
        for j in range(len(a)-i-1):
            if(a[j] > a[j+1]):
                swap(a, j)
    return a
