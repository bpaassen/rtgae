def sort ( a ):
    def swap(a, i):
        a[i], a[i+1] = a[i+1], a[i]
    return a
