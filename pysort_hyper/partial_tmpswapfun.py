def sort ( a ):
    def swap(a, i):
        tmp = a[i]
        a[i] = a[i+1]
        a[i+1] = tmp
    return a
