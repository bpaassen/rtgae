"""
Implements the Boolean formula dataset as described in the paper.
"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import tree
import tree_grammar

# set up the grammar of the Boolean formula domain
alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
nonts = ['S']
start = 'S'
rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

def sample_tree(max_ops = 3):
    """ Returns a tree representing a Boolean formula over the variables
    x and y, sampled according to the following scheme:

    1.  if the tree does not yet contain max_ops binary operations and
        the current parent node is not a 'not' symbol, sample an 'and'
        or 'or' operator with 30% probability each, a 'not' with 10%
        probability, and a 'x' or 'y' with 15% probability each.
    2.  if the tree does not yet contain max_ops binary operations and the
        current parent node is a 'not' symbol, decrease the 'not' probability
        to zero and increase the 'x' and 'y' probability to 20% each.
    3.  if the tree does already contain max_ops binary operations and the
        current parent node is not a 'not' symbol, sample a 'not' operator
        with 20% probability and a 'x' or 'y' with 40% probability each.
    4.  if the tree does already contain max_ops binary operations and the
        current parent node is a 'not' symbol, sample a 'x' or a 'y' with
        50% probability each.


    Parameters
    ----------
    max_ops: int (optional, default = 3)
        the maximum number of binary operations in the tree, which effectively
        limits the tree size as well.


    Returns
    -------
    nodes: list
        The node list of the generated tree.
    adj: list
        The adjacency list of the generated tree.
    """
    remaining_ops = max_ops
    # generate a root node with a placeholder label. We will later
    # remove this root again
    root = tree.Tree('$')
    # initialize a stack which contains all nodes that still require a
    # child.
    stk = [root]
    # generate further nodes until the stack is empty
    while stk:
        par = stk.pop()
        # decide on the sampling probabilities according to the rules above
        if remaining_ops > 0:
            if par._label != 'not':
                r = np.random.choice(5, 1, p = [0.3, 0.3, 0.1, 0.15, 0.15])
            else:
                r = np.random.choice(5, 1, p = [0.3, 0.3, 0.0, 0.2, 0.2])
        else:
            if par._label != 'not':
                r = np.random.choice(5, 1, p = [0., 0., 0.2, 0.4, 0.4])
            else:
                r = np.random.choice(5, 1, p = [0., 0., 0., 0.5, 0.5])
        # generate the new child and update the stack
        if r == 0:
            child = tree.Tree('and')
            stk.append(child)
            stk.append(child)
            remaining_ops -= 1
        elif r == 1:
            child = tree.Tree('or')
            stk.append(child)
            stk.append(child)
            remaining_ops -= 1
        elif r == 2:
            child = tree.Tree('not')
            stk.append(child)
        elif r == 3:
            child = tree.Tree('x')
        elif r == 4:
            child = tree.Tree('y')
        else:
            raise ValueError('internal error!')
        # append the new child to the parent
        par._children.append(child)
    # return the generated tree, which is the first child of our
    # placeholder root node.
    return root._children[0].to_list_format()

def evaluate(nodes, adj, x, y, i = 0):
    """ Logically evaluates a Boolean formula encoded as a tree based on
    the truth values of the variables x and y.

    Parameters
    ----------
    nodes: list
        The node list of the Boolean formula tree.
    adj: list
        The adjacency list of the Boolean formula tree.
    x: bool
        The truth value for x.
    y: bool
        The truth value for y.
    i: int (optional, default = 0)
        The node index from which evaluation should start.


    Returns
    -------
    val: bool
        The truth value of the input formula.
    """
    # evaluate the children.
    child_vals = []
    for j in adj[i]:
        child_vals.append(evaluate(nodes, adj, x, y, j))
    # determine the truth value of the node i itself
    if nodes[i] == 'and':
        return child_vals[0] and child_vals[1]
    if nodes[i] == 'or':
        return child_vals[0] or child_vals[1]
    if nodes[i] == 'not':
        return not child_vals[0]
    if nodes[i] == 'x':
        return x
    if nodes[i] == 'y':
        return y
    raise ValueError('Unknown node type: %s' % nodes[i])
