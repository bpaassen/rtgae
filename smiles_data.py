"""
Implements parts the SMILES grammar of http://opensmiles.org/opensmiles.html
as a regular tree grammar and provides access functions to the zinc data set
as provided by Kusner et al. (2017) under
https://github.com/mkusner/grammarVAE.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
import numpy as np
from antlr4 import InputStream
from antlr4 import CommonTokenStream
from antlr4.tree.Tree import TerminalNode
from antlr4.tree.Tree import ErrorNode
from smiles_parser.smilesLexer import smilesLexer
from smiles_parser.smilesParser import smilesParser
import edist.tree_edits as tree_edits
import tree
import tree_grammar
import recursive_tree_grammar_auto_encoder as rtg_ae

# set up the SMILES tree grammar, i.e. the possible parse trees obtained from
# parsing a SMILES string
alphabet = {
    # meta symbols
    'smiles' : 1,
    # chain bonds
    'single_chain' : 2,
    'double_chain' : 2,
    'triple_chain' : 2,
    # 'quad_chain' : 2,
    'up_chain' : 2,
    'down_chain' : 2,
    'chain_end' : 1,
    # branches
    'branched_atom' : 3,
    'single_ringbond' : 0,
    'double_ringbond' : 0,
    # 'triple_ringbond' : 0,
    # 'quad_ringbond' : 0,
    'up_ringbond' : 0,
    'down_ringbond' : 0,
    'single_branch' : 1,
    'double_branch' : 1,
    'triple_branch' : 1,
    # 'quad_branch' : 1,
    'up_branch' : 1,
    'down_branch' : 1,
    # atom symbols
    'bracket_atom' : 4,#5,
    # 'isotope' : 0, doesn't occur in the zinc database
    'hcount' : 0, 'charge' : 0,
    # chiralities
    '@' : 0, '@@' : 0,
    # Element symbols
    # 'H' : 0, 'He' : 0, 'Li' : 0, 'Be' : 0, 'B' : 0, 'F' : 0, 'Ne' : 0, 'Na' : 0, 'Mg' : 0, 'Al' : 0, 'Si' : 0, 'Cl' : 0, 'Ar' : 0, 'K' : 0, 'Ca' : 0, 'Ti' : 0, 'V' : 0, 'Cr' : 0, 'Mn' : 0, 'Fe' : 0, 'Co' : 0, 'Ni' : 0, 'Cu' : 0, 'Zn' : 0, 'Ga' : 0, 'Ge' : 0, 'As' : 0, 'Se' : 0, 'Br' : 0, 'Kr' : 0, 'Rb' : 0, 'Sr' : 0, 'Y' : 0, 'Zr' : 0, 'Nb' : 0, 'Mo' : 0, 'Tc' : 0, 'Ru' : 0, 'Rh' : 0, 'Pd' : 0, 'Ag' : 0, 'Cd' : 0, 'In' : 0, 'Sn' : 0, 'Sb' : 0, 'Te' : 0, 'I' : 0, 'Xe' : 0, 'Cs' : 0, 'Ba' : 0, 'Hf' : 0, 'Ta' : 0, 'W' : 0, 'Re' : 0, 'Os' : 0, 'Ir' : 0, 'Pt' : 0, 'Au' : 0, 'Hg' : 0, 'Tl' : 0, 'Pb' : 0, 'Bi' : 0, 'Po' : 0, 'At' : 0, 'Rn' : 0, 'Fr' : 0, 'Ra' : 0, 'Rf' : 0, 'Db' : 0, 'Sg' : 0, 'Bh' : 0, 'Hs' : 0, 'Mt' : 0, 'Ds' : 0, 'Rg' : 0, 'Cn' : 0, 'Fl' : 0, 'Lv' : 0, 'La' : 0, 'Ce' : 0, 'Pr' : 0, 'Nd' : 0, 'Pm' : 0, 'Sm' : 0, 'Eu' : 0, 'Gd' : 0, 'Tb' : 0, 'Dy' : 0, 'Ho' : 0, 'Er' : 0, 'Tm' : 0, 'Yb' : 0, 'Lu' : 0, 'Ac' : 0, 'Th' : 0, 'Pa' : 0, 'U' : 0, 'Np' : 0, 'Pu' : 0, 'Am' : 0, 'Cm' : 0, 'Bk' : 0, 'Cf' : 0, 'Es' : 0, 'Fm' : 0, 'Md' : 0, 'No' : 0, 'Lr' : 0,
    'C' : 0, 'N' : 0, 'O' : 0, 'P' : 0, 'S' : 0, 
    # Aromatic symbols
    # 'b': 0, 'c': 0, 'p': 0,, 'se': 0, 'as': 0,
    'n': 0, 'o': 0, 's': 0,
    # Aliphatic Organic
    # 'B_Ali': 0,
    'C_Ali': 0, 'N_Ali': 0, 'O_Ali': 0, 'S_Ali': 0, 'P_Ali': 0, 'F_Ali': 0, 'Cl_Ali': 0, 'Br_Ali': 0, 'I_Ali': 0,
    # Aromatic organic
    # 'b_Aro': 0, 'p_Aro': 0
    'c_Aro': 0, 'n_Aro': 0, 'o_Aro': 0, 's_Aro': 0,
}
nonts = ['S', 'Chain',
    'Branched_Atom',
    'Bracket_atom', # 'Isotope', does not occur
    'Element_symbols', 'Aromatic_symbols', 'Chiral', 'Hcount', 'Charge',
    'Aliphatic_organic', 'Aromatic_organic',
    'Ringbond', 'Branch']
start = 'S'
rules = {
    'S' : [('smiles', ['Chain'])],
    'Chain' : [
        ('single_chain',  ['Chain', 'Branched_Atom']),
        ('double_chain',  ['Chain', 'Branched_Atom']),
        ('triple_chain',  ['Chain', 'Branched_Atom']),
#        ('quad_chain', ['Chain', 'Branched_Atom']), does not occur
        ('up_chain',      ['Chain', 'Branched_Atom']),
        ('down_chain',    ['Chain', 'Branched_Atom']),
        ('chain_end',     ['Branched_Atom'])
    ],
    'Branched_Atom' : [
        ('branched_atom', ['Bracket_atom', 'Ringbond*', 'Branch*']),
        ('branched_atom', ['Aliphatic_organic', 'Ringbond*', 'Branch*']),
        ('branched_atom', ['Aromatic_organic', 'Ringbond*', 'Branch*'])
    ],
    'Bracket_atom' : [
        ('bracket_atom', [#'Isotope?', does not occur
            'Element_symbols', 'Chiral?', 'Hcount?', 'Charge?']),
        ('bracket_atom', [#'Isotope?', does not occur
            'Aromatic_symbols', 'Chiral?', 'Hcount?', 'Charge?'])
        ],
    #'Isotope' : [('isotope', [])],
    'Element_symbols' : [
        # ('H', []), ('He', []), ('Li', []), ('Be', []), ('B', []), ('F', []), ('Ne', []), ('Na', []), ('Mg', []), ('Al', []), ('Si', []), ('Cl', []), ('Ar', []), ('K', []), ('Ca', []), ('Ti', []), ('V', []), ('Cr', []), ('Mn', []), ('Fe', []), ('Co', []), ('Ni', []), ('Cu', []), ('Zn', []), ('Ga', []), ('Ge', []), ('As', []), ('Se', []), ('Br', []), ('Kr', []), ('Rb', []), ('Sr', []), ('Y', []), ('Zr', []), ('Nb', []), ('Mo', []), ('Tc', []), ('Ru', []), ('Rh', []), ('Pd', []), ('Ag', []), ('Cd', []), ('In', []), ('Sn', []), ('Sb', []), ('Te', []), ('I', []), ('Xe', []), ('Cs', []), ('Ba', []), ('Hf', []), ('Ta', []), ('W', []), ('Re', []), ('Os', []), ('Ir', []), ('Pt', []), ('Au', []), ('Hg', []), ('Tl', []), ('Pb', []), ('Bi', []), ('Po', []), ('At', []), ('Rn', []), ('Fr', []), ('Ra', []), ('Rf', []), ('Db', []), ('Sg', []), ('Bh', []), ('Hs', []), ('Mt', []), ('Ds', []), ('Rg', []), ('Cn', []), ('Fl', []), ('Lv', []), ('La', []), ('Ce', []), ('Pr', []), ('Nd', []), ('Pm', []), ('Sm', []), ('Eu', []), ('Gd', []), ('Tb', []), ('Dy', []), ('Ho', []), ('Er', []), ('Tm', []), ('Yb', []), ('Lu', []), ('Ac', []), ('Th', []), ('Pa', []), ('U', []), ('Np', []), ('Pu', []), ('Am', []), ('Cm', []), ('Bk', []), ('Cf', []), ('Es', []), ('Fm', []), ('Md', []), ('No', []), ('Lr', []), do not occur in the zinc database
        ('C', []), ('N', []), ('O', []), ('P', []), ('S', []), 
    ],
    'Aromatic_symbols' : [
        # ('b', []), ('c', []), ('p', []), ('se', []), ('as', []), do not occur in the zinc database
        ('n', []), ('o', []), ('s', [])
    ],
    'Chiral' : [('@', []), ('@@', [])],
    'Hcount' : [('hcount', [])],
    'Charge' : [('charge', [])],
    'Aliphatic_organic' : [
        #('B_Ali', []), does not occur in the zinc database
        ('C_Ali', []), ('N_Ali', []), ('O_Ali', []), ('S_Ali', []), ('P_Ali', []), ('F_Ali', []), ('Cl_Ali', []), ('Br_Ali', []), ('I_Ali', [])
    ],
    'Aromatic_organic' : [
        #('b_Aro', []), ('p_Aro', []), do not occur in the zinc database
        ('c_Aro', []), ('n_Aro', []), ('o_Aro', []), ('s_Aro', [])
    ],
    'Ringbond' : [
        ('single_ringbond', []),
        ('double_ringbond', []),
#        ('triple_ringbond', []), does not occur in the zinc database
#        ('quad_ringbond', []), does not occur in the zinc database
        ('up_ringbond', []),
        ('down_ringbond', [])
    ],
    'Branch' : [
        ('single_branch', ['Chain']),
        ('double_branch', ['Chain']),
        ('triple_branch', ['Chain']),
#        ('quad_branch', ['Chain']), does not occur in the zinc database
        ('up_branch', ['Chain']),
        ('down_branch', ['Chain'])
    ]
}
grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

def parse_smiles(smiles_str):
    """ Parses an input smiles string and returns the corresponding tree.

    Parameters
    ----------
    smiles_str: A SMILES string according to the OpenSMILES standard.

    Returns
    -------
    nodes: list
        The node list of the corresponding tree.
    adj: list
        The adjacency list of the corresponding tree.
    ringbonds: dict
        A map of indices of ringbond nodes to ringbond IDs.
    isotopes: dict
        A map of indices of isotope nodes to isotope numbers.
    hcounts: dict
        A map of indices of hcount nodes to hcounts.
    charges: dict
        A map of indices of charge nodes to charge strengths.

    """
    # first, create an antlr parse tree
    inp = InputStream(smiles_str)
    lexer = smilesLexer(inp)
    stream = CommonTokenStream(lexer)
    parser = smilesParser(stream)
    antlr_tree = parser.smiles()

    # then, convert this tree to our node list/adjacency list format
    nodes = []
    adj   = []
    elements = {}
    ringbonds = {}
    isotopes = {}
    hcounts = {}
    charges = {}
    _convert_tree(antlr_tree, nodes, adj, ringbonds, isotopes, hcounts, charges)
    # return the conversion result
    return nodes, adj, ringbonds, isotopes, hcounts, charges

def _convert_tree(tree, nodes, adj, ringbonds, isotopes, hcounts, charges, p = -1, i = 0):
    # stop at error and terminal nodes
    if isinstance(tree, ErrorNode):
        return i
    elif isinstance(tree, TerminalNode):
        # handle special cases of parent nodes where we need to attach
        # additional information
        if p >= 0:
            if nodes[p].endswith('ringbond'):
                ringbonds[p] = int(tree.getText())
            elif nodes[p] == 'isotope':
                isotopes[p] = int(tree.getText())
            elif nodes[p] == 'chiral':
                nodes[p] = tree.getText()
            elif nodes[p] == 'hcount':
                if tree.getText() == 'H':
                    hcounts[p] = 1
                else:
                    hcounts[p] = int(tree.getText())
            elif nodes[p] == 'charge':
                txt = tree.getText()
                if txt == '+':
                    charges[p] = 1
                elif txt == '++':
                    charges[p] = 2
                elif txt == '-':
                    charges[p] = -1
                elif txt == '--':
                    charges[p] = -2
                else:
                    # check if the parent already has a -1 charge, in which
                    # case we flip the sign
                    if charges[p] == -1:
                        charges[p] = -int(txt)
                    else:
                        charges[p] = int(txt)
            elif nodes[p] == 'aliphatic_organic':
                nodes[p] = tree.getText() + '_Ali'
            elif nodes[p] == 'aromatic_organic':
                nodes[p] = tree.getText() + '_Aro'
            elif nodes[p] in ['element_symbols', 'aromatic_symbols']:
                # if this is an element node, replace the parent node label
                # with the element symbol
                nodes[p] = tree.getText()
        return i
    # extract the node name and append it to the node list
    node_name = type(tree).__name__
    node_name = node_name[:len(node_name)-len('Context')].lower()

    # ignore terminator nodes
    if node_name == 'terminator':
        return i
    # ignore atom nodes and symbol nodes, but treat their children
    if node_name in ['atom', 'symbol']:
        j = i
        for child in tree.getChildren():
            j = _convert_tree(child, nodes, adj, ringbonds, isotopes, hcounts, charges, p, j)
        return j
    # handle bond nodes
    if node_name == 'chain' and len(list(tree.getChildren())) == 1:
        node_name = 'chain_end'
    elif node_name in ['chain', 'ringbond', 'branch']:
        # append 'single' per default
        node_name = 'single_' + node_name
    if node_name == 'bond':
        bond_strength = tree.getText()
        if bond_strength == '-':
            bond_strength = 'single_'
        elif bond_strength == '=':
            bond_strength = 'double_'
        elif bond_strength == '#':
            bond_strength = 'triple_'
        elif bond_strength == '$':
            bond_strength = 'quad_'
        elif bond_strength == '/':
            bond_strength = 'up_'
        elif bond_strength == '\\':
            bond_strength = 'down_'
        else:
            raise ValueError('Invalid bond strength: %s' % bond_strength)
        # re-name the parent node
        nodes[p] = nodes[p].replace('single_', bond_strength)
        return i

    nodes.append(node_name)
    # create a new adjacency list entry
    adj.append([])
    # attach this node to the parent adjacency list entry
    if p >= 0:
        adj[p].append(i)
    # increment the node index
    j = i + 1
    # convert the children
    for child in tree.getChildren():
        j = _convert_tree(child, nodes, adj, ringbonds, isotopes, hcounts, charges, i, j)
    return j

def to_smiles(nodes, adj, ringbonds = None, isotopes = None, hcounts = None, charges = None, i = 0):
    """ Transforms a tree into a SMILES string.

    Parameters
    ----------
    nodes: list
        The node list of the tree.
    adj: list
        The adjacency list of the tree.
    ringbonds: list (default = None)
        A map of indices of ringbond nodes to ringbond IDs.
    isotopes: list (default = None)
        A map of indices of isotope nodes to isotope numbers.
    hcounts: list (default = None)
        A map of indices of hcount nodes to hcounts.
    charges: list (default = None)
        A map of indices of charge nodes to charge strengths.

    Returns
    -------
    smiles_str: str
        A single string containing the SMILES representation of the input tree.

    """
    # recursively process the children
    child_strings = []
    for j in adj[i]:
        child_strings.append(to_smiles(nodes, adj, ringbonds, isotopes, hcounts, charges, j))
    # handle the current node
    if nodes[i] == 'smiles':
        return child_strings[0]
    if nodes[i] == 'single_chain':
        return child_strings[0] + child_strings[1]
    if nodes[i] == 'double_chain':
        return child_strings[0] + '=' + child_strings[1]
    if nodes[i] == 'triple_chain':
        return child_strings[0] + '#' + child_strings[1]
    if nodes[i] == 'quad_chain':
        return child_strings[0] + '$' + child_strings[1]
    if nodes[i] == 'up_chain':
        return child_strings[0] + '/' + child_strings[1]
    if nodes[i] == 'down_chain':
        return child_strings[0] + '\\' + child_strings[1]
    if nodes[i] == 'chain_end':
        return child_strings[0]
    if nodes[i] == 'branched_atom':
        return ''.join(child_strings)
    if nodes[i] == 'atom':
        return child_strings[0]
    if nodes[i] == 'bracket_atom':
        return '[' + ''.join(child_strings) + ']'
    if nodes[i] == 'isotope' and isotopes is not None:
        return str(isotopes[i])
    if nodes[i] == 'symbol':
        return child_strings[0]
    if nodes[i] == 'hcount' and hcounts is not None:
        if hcounts[i] > 1:
            return 'H' + str(hcounts[i])
        else:
            return 'H'
    if nodes[i] == 'charge' and charges is not None:
        if charges[i] == +1:
            return '+'
        if charges[i] == -1:
            return '-'
        if charges[i] > 0:
            return '+' + str(charges[i])
        return str(charges[i])
    if nodes[i].endswith('_Ali'):
        return nodes[i][:-4]
    if nodes[i].endswith('_Aro'):
        return nodes[i][:-4]
    if nodes[i] == 'single_ringbond' and ringbonds is not None:
        return str(ringbonds[i])
    if nodes[i] == 'double_ringbond' and ringbonds is not None:
        return '=' + str(ringbonds[i])
    if nodes[i] == 'triple_ringbond' and ringbonds is not None:
        return '#' + str(ringbonds[i])
    if nodes[i] == 'quad_ringbond' and ringbonds is not None:
        return '$' + str(ringbonds[i])
    if nodes[i] == 'up_ringbond' and ringbonds is not None:
        return '/' + str(ringbonds[i])
    if nodes[i] == 'down_ringbond' and ringbonds is not None:
        return '\\' + str(ringbonds[i])
    if nodes[i] == 'single_branch':
        return '(' + child_strings[0] + ')'
    if nodes[i] == 'double_branch':
        return '(=' + child_strings[0] + ')'
    if nodes[i] == 'triple_branch':
        return '(#' + child_strings[0] + ')'
    if nodes[i] == 'quad_branch':
        return '($' + child_strings[0] + ')'
    if nodes[i] == 'up_branch':
        return '(/' + child_strings[0] + ')'
    if nodes[i] == 'down_branch':
        return '(\\' + child_strings[0] + ')'
    # if the node is any other symbol, return an empty string
    return ""

import torch
from recursive_tree_grammar_auto_encoder import GrammarRule

# code the number of free bonds for each element
num_bonds = {
    'C' : 4, 'c' : 4, 'C_Ali' : 4, 'c_Aro' : 4,
    'N' : 3, 'n' : 3, 'N_Ali' : 3, 'n_Aro' : 3,
    'O' : 2, 'o' : 2, 'O_Ali' : 2, 'o_Aro' : 2,
    'P' : 3, 'p' : 3, 'P_Ali' : 3,
    'S' : 2, 's' : 2, 'S_Ali' : 2, 's_Aro' : 2,
    'F_Ali' : 1, 'Cl_Ali' : 1, 'Br_Ali' : 1, 'I_Ali' : 1,
    'single_chain' : -1, 'single_ringbond' : -1, 'single_branch' : -1,
    'double_chain' : -2, 'double_ringbond' : -2, 'double_branch' : -2,
    'triple_chain' : -3, 'triple_branch' : -3,
    'up_chain' : -1, 'up_ringbond' : -1, 'up_branch' : -1,
    'down_chain' : -1, 'down_ringbond' : -1, 'down_branch' : -1,
    'chain_end' : 0
}

class Decoder(rtg_ae.Decoder):
    """ A version of recursive_tree_grammar_auto_encoder.Decoder which ensures
    that only elements are used which have a sufficient number of free bonds.

    """
    def __init__(self, dim, nonlin = torch.nn.Tanh()):
        super(Decoder, self).__init__(grammar, dim, nonlin = nonlin)

    def decode(self, h, verbose = False, max_size = None):
        """ Produces a tree according based on the rules of self._grammar
        and the given input encoding.

        Parameters
        ----------
        h: class torch.Tensor
            A self._dim dimensional encoding of the tree that should be decoded.
        verbose: bool (default = False)
            If set to true, this prints every rule decision and the
            predicted subtree encoding used for it.
        max_size: int (default = None)
            A maximum tree size to prevent infinite recursion.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        seq: list
            the rule sequence generating the produced tree.

        """
        if len(h) != self._dim:
            raise ValueError('Expected a %d-dimensional input vector, but got %d dimensions!' % (self._dim, len(h)))
        if isinstance(max_size, int):
            max_size = [max_size]
        elif max_size is not None:
            raise ValueError('Expected either an integer or None as max_size, but got %s.' % str(max_size))
        # note that we create the tree in recursive format
        # We start with a placeholder tree to hold our starting symbol
        parent = tree.Tree('$', [tree.Tree(self._grammar._start)])
        seq = []
        # decode the tree recursively
        self._decode(parent, 0, h, seq, verbose, max_size)
        # return the final tree in node list/adjacency list format
        nodes, adj = parent._children[0].to_list_format()
        return nodes, adj, seq

    def _decode(self, par, c, h, seq, verbose = False, max_size = None, free_bonds = 0):
        if max_size is not None :
            if max_size[0] <= 0:
               return
            max_size[0] -= 1

        # retrieve the current nonterminal
        A = par._children[c]._label

        # compute the affinity of all available rules to the current encoding
        # and take the rule which matches best
        scores = self._Vs[A](h)

        # ensure that we only select rules that are chemically plausible in
        # terms of free bonds
        if verbose:
            print('free_bonds: %s' % str(free_bonds))
        mask = None
        if A == 'Chain':
            mask = []
            # only allow bonds that do not reduce the number of free bonds
            # below -4 (i.e. the maximum number that can still be provided
            # by carbon)
            for r in range(len(self._grammar._rules[A])):
                bond = self._grammar._rules[A][r][0]
                if free_bonds + num_bonds[bond] < -4:
                    continue
                mask.append(r)
        if A in ['Ringbond', 'Branch']:
            mask = []
            # only allow bonds which do not reduce the number of free bonds
            # below zero
            for r in range(len(self._grammar._rules[A])):
                bond = self._grammar._rules[A][r][0]
                if free_bonds + num_bonds[bond] < 0:
                    continue
                mask.append(r)
        elif A in ['Ringbond*', 'Branch*']:
            # do not create further bonds if already all spots are taken
            if free_bonds <= 0:
                mask = [0]
        elif A in ['Element_symbols', 'Aromatic_symbols', 'Aliphatic_organic', 'Aromatic_organic']:
            # mask out all elements that do not make sense because they do not
            # provide enough bonds
            mask = []
            for r in range(len(self._grammar._rules[A])):
                element = self._grammar._rules[A][r][0]
                if free_bonds + num_bonds[element] < 0:
                    continue
                mask.append(r)
        if mask is not None:
            if verbose:
                print('Only permitted the following rules due to bond restrictions: %s' % str(mask))
            r_max = mask[torch.argmax(scores[mask]).item()]
        else:
            r_max = torch.argmax(scores).item()
        seq.append(r_max)

        if isinstance(A, str):
            # if the nonterminal ends with a *, we have to handle a list
            if A.endswith('*'):
                lst_node = par._children[c]
                # here, two rules are possible:
                if r_max == 0:
                    # the zeroth rule completes the list and means
                    # that we replace the entire tree node with the
                    # child list
                    par._children[c] = lst_node._children
                    if verbose:
                        print('decided to close starred nonterminal %s based on code %s' % (A, str(h.tolist())))
                else:
                    # the first rule continues the list, which means
                    # that we append a new child and process that
                    if verbose:
                        print('decided to continue starred nonterminal %s based on code %s' % (A, str(h.tolist())))
                    lst_node._children.append(tree.Tree(A[:-1]))
                    # extract the encoding for the current child and decode it
                    h_c = self._nonlin(self._nont_star_layers[A](h))
                    free_bonds = self._decode(lst_node, len(lst_node._children)-1, h_c, seq, verbose, max_size, free_bonds)
                    # update h and check the starred nonterminal again
                    # TODO this may be smarter via a recurrent matrix
                    h = h - h_c
                    free_bonds = self._decode(par, c, h, seq, verbose, max_size, free_bonds)
                return free_bonds
            # if the nonterminal ands with a ?, we have to handle an optional
            # node
            elif A.endswith('?'):
                # here, two rules are possible
                if r_max == 0:
                    # the zeroth rule means we replace the nonterminal with
                    # None
                    par._children[c] = None
                    if verbose:
                        print('decided to omit optional nonterminal %s based on code %s' % (A, str(h.tolist())))
                else:
                    # the first rule means we replace the nonterminal
                    # with its non-optional version and process it then
                    if verbose:
                        print('decided to use optional nonterminal %s based on code %s' % (A, str(h.tolist())))
                    par._children[c]._label = A[:-1]
                    A = A[:-1]
                    self._decode(par, c, h, seq, verbose, max_size)
                return free_bonds
        # if the nonterminal is neither starred, nor optional, use the
        # regular processing
        rule = self._rules[A][r_max]
        sym, children = self._grammar._rules[A][r_max]
        if verbose:
            print('decided to apply rule %s -> %s(%s) based on code %s' % (A, sym, str(children), str(h.tolist())))

        # store the old number of free bonds for the Chain construct
        if A == 'Chain' and sym != 'chain_end':
            old_free_bonds = free_bonds
            free_bonds = 0
        # adjust the number of free bonds
        if sym in num_bonds:
            free_bonds += num_bonds[sym]

        if verbose:
            print('new number of free bonds: %d' % free_bonds)

        # replace A in the input tree with sym(rights)
        subtree = tree.Tree(sym)
        par._children[c] = subtree
        # recursively process all children with a recurrent scheme
        for c in range(len(children)):
            subtree._children.append(tree.Tree(children[c]))
            # generate the predicted encoding for the current child
            h_c = self._nonlin(rule._child_layers[c](h))
            # recursively process the current child
            child_bonds = self._decode(subtree, c, h_c, seq, verbose, max_size, free_bonds)
            # update h; TODO this may be smarter via a recurrent matrix
            h = h - h_c
            if A == 'Chain' and sym != 'chain_end':
                free_bonds += old_free_bonds
            elif A == 'Branched_Atom':
                free_bonds = child_bonds

        # return the the new number of required bonds
        return free_bonds


##### objective function for optimization #####

from rdkit import Chem
from rdkit.Chem import Descriptors
import sascorer
import networkx as nx
from rdkit.Chem import rdmolops

def objective_function(nodes, adj):
    """ Computes the objective function value for optimization.
    This value mostly follows the work of Kusner et al. (2017), albeit
    without batch-wise normalization. Thus, small deviations from Kusner's
    reference score can not be avoided.

    Parameters
    ----------
    nodes: list
        The node list of a molecule.
    adj: list
        The adjacency list of a molecule.

    Returns
    -------
    score: float
        The objective function value, where higher values are better.

    """
    # in a first step, heuristically assign indices to the ringbonds.
    nodes, adj, ringbonds = ringbond_heuristic(nodes, adj)

    # transform the input tree to a smiles string
    smiles_string = to_smiles(nodes, adj, ringbonds = ringbonds)

    # compute the score
    return compute_score(smiles_string)

def ringbond_heuristic(nodes, adj):
    """ Heuristically generates ringbond indices for the given molecular tree.
    In particular, this heuristic closes ringbonds as early as possible
    and adds ringbonds in case there are remaining ones at the end.

    Parameters
    ----------
    nodes: list
        The node list of a molecule.
    adj: list
        The adjacency list of a molecule.

    Returns
    -------
    nodes: list
        The node list of a repaired molecule.
    adj: list
        The adjacency list of a repaired molecule.
    ringbonds: dictionary
        A mapping from ringbond node indices to ringbond indices.

    """
    # initialize the ringbond dictionary
    ringbonds = {}
    # initialize a stack for all ringbond types
    open_ringbonds = {}
    open_ringbond_idxs = set()
    for i in range(len(nodes)):
        # if we find a branched_atom node, inspect all ringbonds
        if nodes[i] != 'branched_atom':
            continue
        # initialize the ringbonds opened at this atom
        local_rings = []
        # iterate over all children
        for j in adj[i]:
            if nodes[j].endswith('ringbond'):
                if nodes[j] in open_ringbonds and len(open_ringbonds[nodes[j]]) > 0:
                    # close an existing ring if possible
                    ringbonds[j] = open_ringbonds[nodes[j]].pop()
                    open_ringbond_idxs.remove(ringbonds[j])
                else:
                    # otherwise open a new ring
                    k = 1
                    while k in open_ringbond_idxs:
                        k += 1
                    local_rings.append((nodes[j], k))
                    ringbonds[j] = k
        # append all newly opened ringbonds to the data structure
        for ringbond_type, k in local_rings:
            open_ringbonds.setdefault(ringbond_type, []).append(k)
            open_ringbond_idxs.add(k)

    if len(open_ringbond_idxs) > 0:
        # if we need to repair ringbonds, compute the number of free bonds for
        # each atom
        free_bonds = compute_free_bonds(nodes, adj)

        nodes = copy.copy(nodes)
        adj   = copy.deepcopy(adj)

        repairs = []
        for k in sorted(list(open_ringbond_idxs)):
            # if we have remaining bonds open, repair that by searching, from the
            # end of the molecule, for the first atom that still has sufficient
            # free bonds.

            # identify the number of bonds we need
            start_idx = -1
            for key in ringbonds:
                if ringbonds[key] == k and key > start_idx:
                    start_idx = key
            required_bonds = -num_bonds[nodes[start_idx]]

            # then find the last atom which has the required number of free bonds
            end_idx = None
            for i in range(len(nodes)-1, -1, -1):
                if i not in free_bonds or i == start_idx or free_bonds[i] < required_bonds:
                    continue
                end_idx = i
                break
            if end_idx is None:
                raise ValueError('Ringbond %d from node %d could not be repaired' % (num_rings, start_idx))
            # note the newly to be inserted ringnode
            c = len(adj[end_idx])
            while c > 0 and nodes[adj[end_idx][c-1]].endswith('branch'):
                c -= 1

            repairs.append((end_idx, c, nodes[start_idx], k))
            # decrease the ringbond index
            open_ringbond_idxs.remove(k)
        # sort the repairs in (lexicographically) ascending order
        repairs.sort()
        # then apply the repairs, starting from the biggest index
        for r in range(len(repairs)-1, -1, -1):
            p, c, label, k = repairs[r]
            if(c < len(adj[p])):
                idx = adj[p][c]
            else:
                # if this child does not exist yet, use the index of the
                # right sibling of the parent node
                # this index can be retrieved by first getting the outermost
                # right leaf of p.
                orl = p
                while(adj[orl]):
                    orl = adj[orl][-1]
                # and then incrementing it by 1
                idx = orl + 1
            # insert the new child to the parent
            adj[p].insert(c, idx)
            # insert a new entry in the node list
            nodes.insert(idx, label)
            # insert a new entry into the adjacency list
            adj.insert(idx, [])
            # increment all indices >= idx
            for i in range(len(adj)):
                for j in range(len(adj[i])):
                    if(adj[i][j] >= idx):
                        adj[i][j] += 1
            # decrement the actual new index again
            if(p >= 0):
                adj[p][c] -= 1
            # increment all ringbond indices >= idx
            new_ringbonds = {}
            for key in ringbonds:
                if key >= idx:
                    new_ringbonds[key+1] = ringbonds[key]
                else:
                    new_ringbonds[key] = ringbonds[key]
            new_ringbonds[idx] = k
            ringbonds = new_ringbonds

    return nodes, adj, ringbonds


def compute_free_bonds(nodes, adj):
    """ Computes the free bonds for each atom node in the given molecule
    tree.

    Parameters
    ----------
    nodes: list
        The node list of a molecule.
    adj: list
        The adjacency list of a molecule.

    Returns
    -------
    free_bonds: dict
        A mapping from branched_atom node indices to free bonds.

    """
    # reconstruct the parent map from the adjacency list
    pi = -np.ones(len(nodes), dtype=int)
    for i in range(len(adj)):
        for j in adj[i]:
            pi[j] = i

    # initialize output map
    free_bonds = {}

    # iterate over all branched_atom nodes
    for i in range(len(nodes)):
        if nodes[i] != 'branched_atom':
            continue
        # for these nodes, the number of free bonds is the number of free bonds
        # of the kind of atom ...
        atom_idx = i
        while len(adj[atom_idx]) > 0:
            atom_idx = adj[atom_idx][0]

        fb = num_bonds[nodes[atom_idx]]

        if pi[i] >= 0:
            # ... minus the number of bonds occupied by the parent chain
            parent_chain = nodes[pi[i]]
            fb += num_bonds[parent_chain] if parent_chain in num_bonds else 0

            # ... minus the number of bonds occupied by the grandpatent chain
            if pi[pi[i]] >= 0 and nodes[pi[pi[i]]] in num_bonds:
                fb += num_bonds[nodes[pi[pi[i]]]]

        # ... minus the number of bonds occupied by child ringbonds and
        # branches
        for c in range(1, len(adj[i])):
            j = adj[i][c]
            fb += num_bonds[nodes[j]]

        # store the final number
        free_bonds[i] = fb
    # return the output map
    return free_bonds


# these are empirically computed values of the average logp, sa, and cycle
# score as well as standard deviation within the smiles database section of
# Kusner et al. These values are needed to reproduce the objective function
# as defined by Kusner and colleagues.
_logp_mean  =  2.4570953396190123
_logp_std   =  1.434324401111988
_sa_mean    = -3.0532633004308822
_sa_std     =  0.8348133328896592
_cycle_mean = -0.04826903341671477
_cycle_std  =  0.2868627438746916

def compute_score(smiles_str):
    """ Computes the objective function value for optimization.
    This value follows the implementation of of Kusner et al. (2017) as given
    in

    https://github.com/mkusner/grammarVAE/blob/master/molecule_optimization/simulation1/grammar/run_bo.py

    We note, however, that the values we get weirdly deviate from the
    values reported in Kusner et al. (2017) in that we obtain the same values
    as reported there in Table 4, but in a different order. This may be due to
    a misprint, though.

    Parameters
    ----------
    smiles_str: str
        a SMILES string.

    Returns
    -------
    score: float
        The objective function value, where higher values are better.

    """
    # parse the smiles string into a molecule object using the rdkit library
    m = Chem.MolFromSmiles(smiles_str)
    # compute logp value
    logp    = Descriptors.MolLogP(m)
    # ... synthetic accessibility according to the Novartis scheme
    sascore = -sascorer.calculateScore(m)
    # ... and the cycle length
    cycle_list = nx.cycle_basis(nx.Graph(rdmolops.GetAdjacencyMatrix(m)))
    if len(cycle_list) == 0:
        cycle_length = 0
    else:
        cycle_length = max([ len(j) for j in cycle_list ])
    if cycle_length <= 6:
        cycle_length = 0
    else:
        cycle_length = cycle_length - 6

    cycle_score = -cycle_length

    # z-normalize all scores according to the entire smiles database
    logp_normalized        = (logp - _logp_mean) / _logp_std
    sascore_normalized     = (sascore - _sa_mean) / _sa_std
    cycle_score_normalized = (cycle_score - _cycle_mean) / _cycle_std

    # the end result is the sum of these three scores
    score       = logp_normalized + sascore_normalized + cycle_score_normalized

    return score

# This code is to compute the normalization constants for the objective
# function, such that logp, sa score and cycle score are weighted just as in
# the paper of Kusner et al., 2017

#def compute_normalization_scores(verbose = True):
#    logps = []
#    sascores = []
#    cycle_scores =[]

#    l = 0
#    with open('250k_rndm_zinc_drugs_clean.smi') as f:
#        for line in f:
#            # parse the smiles string into a molecule object using the rdkit library
#            m = Chem.MolFromSmiles(line)
#            # compute logp value
#            logp    = Descriptors.MolLogP(m)
#            # ... synthetic accessibility according to the Novartis scheme
#            sascore = - sascorer.calculateScore(m)
#            # ... and the cycle length
#            cycle_list = nx.cycle_basis(nx.Graph(rdmolops.GetAdjacencyMatrix(m)))
#            if len(cycle_list) == 0:
#                cycle_length = 0
#            else:
#                cycle_length = max([ len(j) for j in cycle_list ])
#            if cycle_length <= 6:
#                cycle_length = 0
#            else:
#                cycle_length = cycle_length - 6

#            cycle_score = -cycle_length

#            logps.append(logp)
#            sascores.append(sascore)
#            cycle_scores.append(cycle_score)

#            l += 1
#            if verbose and l % 1000 == 0:
#                print('Analyzed %d molecules' % l)
#                print('Current logp: %g +- %g' % (np.mean(logps), np.std(logps)))
#                print('Current sascore: %g +- %g' % (np.mean(sascores), np.std(sascores)))
#                print('Current cycle_score: %g +- %g' % (np.mean(cycle_scores), np.std(cycle_scores)))

#    return np.mean(logps), np.std(logps), np.mean(sascores), np.std(sascores), np.mean(cycle_scores), np.std(cycle_scores)
